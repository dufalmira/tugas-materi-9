<?php 
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $objek = new Animal("shaun");
    echo "Nama : " . $objek->name . "<br>";
    echo "legs : " . $objek->legs . "<br>";
    echo "cold blooded : " . $objek->cold_blooded . "<br>";
    echo "<br>";
    
    $objek2 = new Frog("buduk");
    echo "Nama : " . $objek2->name . "<br>";
    echo "legs : " . $objek2->legs . "<br>";
    echo "cold blooded : " . $objek2->cold_blooded . "<br>";
    $objek2->jump();
    echo "<br>";
    echo "<br>";
    
    $objek3 = new Ape("kera sakti");
    echo "Nama : " . $objek3->name . "<br>";
    echo "legs : " . $objek3->legs . "<br>";
    echo "cold blooded : " . $objek3->cold_blooded . "<br>";
    $objek3->yell();
?>